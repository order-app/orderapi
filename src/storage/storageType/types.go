package storageType

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Order struct {
	Id         primitive.ObjectID `json:"_id" bson:"_id"`
	CustomerId primitive.ObjectID `json:"customerId" bson:"customerId"`
	Quantity   int                `json:"quantity" bson:"quantity"`
	Price      float64            `json:"price" bson:"price"`
	Status     Status             `json:"status" bson:"status"`
	Address    Address            `json:"address" bson:"address"`
	Product    Product            `json:"product" bson:"product"`
	CreatedAt  time.Time          `json:"createdAt" bson:"createdAt"`
	UpdatedAt  time.Time          `json:"updatedAt" bson:"updatedAt"`
}

type Address struct {
	AddressLine string `json:"addressLine" bson:"addressLine"`
	City        string `json:"city" bson:"city"`
	Country     string `json:"country" bson:"country"`
	CityCode    int    `json:"cityCode" bson:"cityCode"`
}

type Product struct {
	Id       primitive.ObjectID `json:"productId" bson:"productId"`
	ImageUrl string             `json:"imageUrl" bson:"imageUrl"`
	Name     string             `json:"name" bson:"name"`
}

type Status int

const (
	Pending Status = iota
	Active
	Suspended
	Rejected
)

func (s Status) String() string {
	return [...]string{"Pending", "Active", "Suspended", "Rejected"}[s]
}

func (s Status) EnumIndex() int {
	return int(s)
}
