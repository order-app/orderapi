package helper

import (
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/url"
	errors2 "orderservice/src/pkg/helper/errors"
	"strconv"
)

func MakeFilter(values url.Values) map[string]interface{} {
	var filter = make(map[string]interface{})
	for i, v := range values {
		filter[i] = v[0]
		switch {
		case i == "_id":
			id, err := primitive.ObjectIDFromHex(filter["_id"].(string))
			if err != nil {
				log.Errorf("Cannot convert to ObjectId")
				errors2.Panic(errors2.ConvertIdError)
			}
			filter["_id"] = id
		case i == "customerId":
			id, err := primitive.ObjectIDFromHex(filter["customerId"].(string))
			if err != nil {
				log.Errorf("Cannot convert to customer ObjectId")
				errors2.Panic(errors2.CustomerIdError)
			}
			filter["customerId"] = id
		case i == "quantity":
			quantity, err := strconv.Atoi(filter["quantity"].(string))
			if err != nil {
				log.Errorf("Convert error : %s", err)
				errors2.Panic(errors2.ConvertError)
			}
			filter[i] = quantity
		case i == "price":
			price, err := strconv.ParseFloat(filter["price"].(string), 64)
			if err != nil {
				log.Errorf("Convert error : %s", err)
				errors2.Panic(errors2.ConvertError)
			}
			filter[i] = price
		case i == "address.cityCode":
			cityCode, err := strconv.Atoi(filter["address.cityCode"].(string))
			if err != nil {
				log.Errorf("Convert error : %s", err)
				errors2.Panic(errors2.ConvertError)
			}
			filter[i] = cityCode
		case i == "product.productId":
			id, err := primitive.ObjectIDFromHex(filter["product.productId"].(string))
			if err != nil {
				log.Errorf("Cannot convert to product ObjectId")
				errors2.Panic(errors2.ConvertError)
			}
			filter["product.productId"] = id
		}
	}
	return filter
}
