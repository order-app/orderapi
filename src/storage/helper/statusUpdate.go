package helper

import (
	"go.mongodb.org/mongo-driver/bson"
	"orderservice/src/storage/storageType"
	"strings"
	"time"
)

func SetUpdateForStatus(status string) interface{} {
	var update bson.M
	if status == strings.ToLower(storageType.Pending.String()) {
		update = bson.M{"$set": bson.M{"status": storageType.Pending.EnumIndex(),
			"updatedAt": time.Now()}}
	} else if status == strings.ToLower(storageType.Active.String()) {
		update = bson.M{"$set": bson.M{"status": storageType.Active.EnumIndex(),
			"updatedAt": time.Now()}}
	} else if status == strings.ToLower(storageType.Suspended.String()) {
		update = bson.M{"$set": bson.M{"status": storageType.Suspended.EnumIndex(),
			"updatedAt": time.Now()}}
	} else if status == strings.ToLower(storageType.Rejected.String()) {
		update = bson.M{"$set": bson.M{"status": storageType.Rejected.EnumIndex(),
			"updatedAt": time.Now()}}
	}
	return update
}
