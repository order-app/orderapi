package mongodb

import (
	"context"
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"orderservice/src/storage/mongodb/dbiface"
	"orderservice/src/storage/storageType"
	"time"
)

type Repository struct {
	col dbiface.MongoCollection
}

func NewRepository(col dbiface.MongoCollection) Repository {
	return Repository{col: col}
}

func (r *Repository) FindById(id primitive.ObjectID) *storageType.Order {
	var order storageType.Order
	filter := bson.M{"_id": id}
	res := r.col.FindOne(context.Background(), filter)
	if err := res.Decode(&order); err != nil {
		log.Errorf("Not found in database with id : %s err : %s", id, err)
		return nil
	}
	return &order
}

func (r *Repository) FindMany(filter map[string]interface{}, options *options.FindOptions) (int64, *[]storageType.Order) {
	c := make(chan int64)
	errChan := make(chan error)
	go r.countDocument(filter, c, errChan)
	res, colErr := r.col.Find(context.Background(), bson.M(filter), options)
	if colErr != nil {
		log.Errorf("Find error : %s", colErr)
		return 0, nil
	}
	var orders []storageType.Order
	if cursErr := res.All(context.Background(), &orders); cursErr != nil {
		log.Errorf("Unable to read the cursor : %v")
		return 0, nil
	}
	//total, _ := r.col.CountDocuments(context.Background(), filter)
	countErr := <-errChan
	if countErr != nil {
		log.Errorf("Count Documents error : %s", countErr.Error())
		return 0, nil
	}
	total := <-c
	return total, &orders
}

func (r *Repository) countDocument(filter interface{}, c chan int64, errChan chan error) {
	total, err := r.col.CountDocuments(context.Background(), filter)
	if err != nil {
		errChan <- err
	}
	close(errChan)
	c <- total
}

func (r *Repository) InsertOne(order storageType.Order) interface{} {
	order.Id = primitive.NewObjectID()
	order.Product.Id = primitive.NewObjectID()
	order.CreatedAt = time.Now()
	order.UpdatedAt = time.Now()
	res, err := r.col.InsertOne(context.Background(), order)
	if err != nil {
		log.Errorf("Insert error : %s", err)
		return nil
	}
	return res.InsertedID
}

func (r *Repository) DeleteOne(id primitive.ObjectID) int64 {
	filter := bson.M{"_id": id}
	res, _ := r.col.DeleteOne(context.Background(), filter)
	return res.DeletedCount
}

func (r *Repository) UpdateOne(reqOrder storageType.Order) int64 {
	reqOrder.UpdatedAt = time.Now()
	filter := bson.M{"_id": reqOrder.Id}
	update := bson.M{"$set": bson.M{"customerId": reqOrder.CustomerId, "quantity": reqOrder.Quantity,
		"price": reqOrder.Price, "status": reqOrder.Status, "address.addressLine": reqOrder.Address.AddressLine,
		"address.city": reqOrder.Address.City, "address.country": reqOrder.Address.Country,
		"address.cityCode": reqOrder.Address.CityCode, "product.imageUrl": reqOrder.Product.ImageUrl,
		"product.name": reqOrder.Product.Name, "updatedAt": reqOrder.UpdatedAt}}
	res, _ := r.col.UpdateOne(context.Background(), filter, update)
	return res.ModifiedCount
}

func (r *Repository) UpdateStatus(id primitive.ObjectID, update interface{}) int64 {
	filter := bson.M{"_id": id}
	res, _ := r.col.UpdateOne(context.Background(), filter, update)
	return res.ModifiedCount
}
