package client

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/labstack/gommon/log"
	"net/http"
	"orderservice/src/handler/type"
	"orderservice/src/pkg/config"
	errors2 "orderservice/src/pkg/helper/errors"
	_errType "orderservice/src/pkg/helper/errors/type"
	"time"
)

type Client struct {
	BaseURI    string
	HTTPClient *http.Client
}

func NewClient(client config.Client) *Client {
	return &Client{
		BaseURI: client.BaseURI,
		HTTPClient: &http.Client{
			Timeout: time.Minute,
		},
	}
}

func (c *Client) GetIsCustomerIdValid(id string) {
	req, err := http.NewRequestWithContext(context.Background(), "GET", fmt.Sprintf("%s/customer/valid?id=%s", c.BaseURI, id), nil)
	if err != nil {
		log.Errorf("NewRequest error : %s", err)
		errors2.Panic(errors2.ClientError)
	}
	res := c.sendRequest(req)
	if res != true {
		errors2.Panic(errors2.CustomerIdError)
	}
}

func (c *Client) GetCustomerServiceHealthy() bool {
	req, err := http.NewRequestWithContext(context.Background(), "GET", fmt.Sprintf("%s/quickhealthy", c.BaseURI), nil)
	if err != nil {
		log.Errorf("NewRequest error : %s", err)
		errors2.Panic(errors2.ClientError)
	}
	res := c.sendRequest(req)
	return res
}

func (c *Client) sendRequest(req *http.Request) bool {
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "application/json; charset=utf-8")

	res, doErr := c.HTTPClient.Do(req)
	if doErr != nil {
		log.Errorf("HttpClient Do error : %s", doErr)
		errors2.Panic(errors2.ClientError)
	}

	defer res.Body.Close()

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes *_errType.ErrorType
		if dErr := json.NewDecoder(res.Body).Decode(&errRes); dErr != nil {
			log.Errorf("Send request decode error message error : %s", dErr)
			errors2.Panic(errors2.ClientError)
		}
		errors2.Panic(_errType.ErrorType{Code: errRes.Code, Message: errRes.Message})
	}
	var fullResponse _type.QuickResponseType
	if err := json.NewDecoder(res.Body).Decode(&fullResponse); err != nil {
		log.Errorf("Send request response decode error : %s", err)
		errors2.Panic(errors2.ClientError)
	}
	return fullResponse.Result
}
