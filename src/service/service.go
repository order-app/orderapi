package service

import (
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	errors2 "orderservice/src/pkg/helper/errors"
	"orderservice/src/storage/mongodb"
	"orderservice/src/storage/storageType"
)

type Service struct {
	repo mongodb.Repository
}

func NewService(repo mongodb.Repository) Service {
	return Service{repo: repo}
}

func (s *Service) FindById(id primitive.ObjectID) *storageType.Order {
	res := s.repo.FindById(id)
	if res == nil {
		errors2.Panic(errors2.OrderNotFoundError)
	}
	return res
}

func (s *Service) FindOrders(filter map[string]interface{}, options *options.FindOptions) (int64, *[]storageType.Order) {
	total, res := s.repo.FindMany(filter, options)
	if len(*res) == 0 {
		errors2.Panic(errors2.OrderNotFoundError)
	}
	return total, res
}

func (s *Service) InsertOrder(order storageType.Order) interface{} {
	res := s.repo.InsertOne(order)
	if res == nil {
		errors2.Panic(errors2.MongoInsertOneError)
	}
	return res
}

func (s *Service) UpdateOrder(order storageType.Order) {
	res := s.repo.UpdateOne(order)
	if res == 0 {
		log.Errorf("Not found in database with id : %s", order.Id)
		errors2.Panic(errors2.OrderNotFoundError)
	}
}

func (s *Service) DeleteOneOrder(id primitive.ObjectID) {
	res := s.repo.DeleteOne(id)
	if res == 0 {
		log.Errorf("Not found in database with id : %s", id)
		errors2.Panic(errors2.OrderNotFoundError)
	}
}

func (s *Service) ChangeStatus(id primitive.ObjectID, update interface{}) {
	res := s.repo.UpdateStatus(id, update)
	if res == 0 {
		log.Errorf("Not found in database with id : %s", id)
		errors2.Panic(errors2.OrderNotFoundError)
	}
}
