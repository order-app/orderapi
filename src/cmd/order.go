package cmd

import (
	"context"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	echoSwagger "github.com/swaggo/echo-swagger"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"orderservice/src/client"
	"orderservice/src/docs"
	"orderservice/src/handler"
	"orderservice/src/orderhealthcheck"
	"orderservice/src/pkg/config"
	"orderservice/src/pkg/healthcheck"
	custommiddleware "orderservice/src/pkg/helper/middleware"
	"orderservice/src/service"
	"orderservice/src/storage/mongodb"
)

var (
	mclient     *mongo.Client
	clientCfg   config.Client
	cfg         config.Config
	mongoClient *mongo.Client
	db          *mongo.Database
	col         *mongo.Collection
)

func init() {
	cfg = config.EnvConfig["local"]
	clientCfg = config.ClientConfig["local"]
	mongoURI := fmt.Sprintf("mongodb+srv://%s:%s@cluster0.ng3fk.mongodb.net/%s?retryWrites=true&w=majority", cfg.OrderUserName, cfg.OrderPassword, cfg.DBName)
	clientOpts := options.Client().ApplyURI(mongoURI)
	mongoClient, conErr := mongo.Connect(context.Background(), clientOpts)
	if conErr != nil {
		log.Errorf("Somethings went wrong when connecting mongodb.")
	}
	db = mongoClient.Database(cfg.DBName)
	col = db.Collection(cfg.OrderCollectionName)
	mclient = mongoClient
}

func Execute() {
	e := echo.New()
	repository := mongodb.NewRepository(col)
	orderService := service.NewService(repository)
	orderClient := client.NewClient(clientCfg)
	orderHandler := handler.NewOrderHandler(orderService, *orderClient)
	healthCheck := healthcheck.NewHealthChecks()
	mongoCheck := orderhealthcheck.NewMongoHealthCheck(mclient)
	customerCheck := orderhealthcheck.NewCustomerHealthCheck(orderClient)
	healthCheck.AddHealthChecks("mongoDB", &mongoCheck)
	healthCheck.AddHealthChecks("customerApi", &customerCheck)
	healthHandler := handler.NewHealthyHandler(healthCheck)
	e.GET("/healthy", healthHandler.CheckServicesHealth)
	Route(e, orderHandler)
	SwaggerSettings()
	e.Start(fmt.Sprintf("%s:%s", cfg.Host, cfg.Port))
}

func Route(e *echo.Echo, handler handler.OrderHandler) {
	e.Use(custommiddleware.Recovery)
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.PUT, echo.POST, echo.DELETE},
	}))
	e.GET("/order/:id", handler.FindOrderById)
	e.GET("/order", handler.FindOrders)
	e.POST("/order", handler.CreateOrder)
	e.DELETE("/order/:id", handler.DeleteOrder)
	e.PUT("/order/:id", handler.UpdateOrder)
	e.PUT("/order/status/:id", handler.ChangeOrderStatus)
	e.GET("/swagger/*", echoSwagger.WrapHandler)
}

func SwaggerSettings() {
	docs.SwaggerInfo.Title = "Order Service"
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Description = "Order Service"
	docs.SwaggerInfo.Host = fmt.Sprintf("%s:%s", cfg.Host, cfg.Port)
	docs.SwaggerInfo.Schemes = []string{"http"}
}
