package errors

import (
	"orderservice/src/pkg/helper/errors/type"
)

func Panic(error _type.ErrorType) {
	panic(&_type.ErrorType{
		Code:    error.Code,
		Message: error.Message,
	})
}
