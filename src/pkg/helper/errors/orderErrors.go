package errors

import (
	"net/http"
	"orderservice/src/pkg/helper/errors/type"
)

var (
	OrderNotFoundError = _type.ErrorType{
		Code:    http.StatusNotFound,
		Message: "Order not found.",
	}

	OrderBindError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "Unable to bind the request body.",
	}

	ClientError = _type.ErrorType{
		Code:    http.StatusInternalServerError,
		Message: "HttpClient error.",
	}

	ConvertIdError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "Id is not valid. Please write valid Id.",
	}
	CustomerIdError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "CustomerId is not valid. Please write valid Id.",
	}

	MongoInsertOneError = _type.ErrorType{
		Code:    http.StatusInternalServerError,
		Message: "InsertOne error.",
	}

	MongoDeleteOneError = _type.ErrorType{
		Code:    http.StatusInternalServerError,
		Message: "DeleteOne error.",
	}

	ConvertError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "Convert error.",
	}

	QuantityValidationError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "Quantity is required, it can not be lower than 1.",
	}

	PriceValidationError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "Price is required, it can not be lower than 1.",
	}

	StatusValidationError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "Status is required, it can not be empty.",
	}

	ImageUrlValidationError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "ImageUrl is required, it can not be empty.",
	}

	AddressLineValidationError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "AddressLine is required, it can not be empty.",
	}

	CountryValidationError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "Country is required, it can not be empty.",
	}

	CityValidationError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "City is required, it can not be empty.",
	}

	ProductNameValidationError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "ProductName is required, it can not be empty.",
	}

	CityCodeValidationError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "CityCode is required, it can not be lower than 1.",
	}

	ChangeStatusValidationError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "Status field can take : pending, active, suspended, rejected",
	}
)
