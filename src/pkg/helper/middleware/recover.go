package middleware

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"orderservice/src/pkg/helper/errors/type"
)

func Recovery(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		defer func() {
			err := recover()
			if err != nil {
				resErr, cErr := err.(*_type.ErrorType)
				if cErr != true {
					log.Errorf(resErr.Message)
				}
				c.JSON(resErr.Code, resErr)
			}
		}()
		return next(c)
	}
}
