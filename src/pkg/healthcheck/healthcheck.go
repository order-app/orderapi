package healthcheck

import "time"

type HealthCheck interface {
	Check() bool
}

type HealthChecks struct {
	healthChecks map[string]HealthCheck
}

func NewHealthChecks() HealthChecks {
	var healthCheckServices = make(map[string]HealthCheck)
	return HealthChecks{healthChecks: healthCheckServices}
}

func (h *HealthChecks) AddHealthChecks(name string, check HealthCheck) {
	h.healthChecks[name] = check
}

func (h *HealthChecks) CheckServices() map[string]map[bool]time.Time {
	var serviceResults = make(map[string]map[bool]time.Time)
	var result = make(map[bool]time.Time)
	for i, s := range h.healthChecks {
		res := s.Check()
		result[res] = time.Now()
		serviceResults[i] = result
	}
	return serviceResults
}

type Status int

const (
	Healthy Status = iota
	Unhealthy
)

func (s Status) String() string {
	return [...]string{"Healthy", "Unhealthy"}[s]
}

func (s Status) EnumIndex() int {
	return int(s)
}
