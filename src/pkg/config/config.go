package config

type Client struct {
	BaseURI string
}

type Config struct {
	Host                string
	Port                string
	OrderUserName       string
	OrderPassword       string
	DBName              string
	OrderCollectionName string
}

var EnvConfig = map[string]Config{
	"local": {
		Host:                "localhost",
		Port:                "8090",
		OrderUserName:       "ordercustomer",
		OrderPassword:       "12345",
		DBName:              "orderapplicationDB",
		OrderCollectionName: "orders",
	},
}

var ClientConfig = map[string]Client{
	"local": {
		BaseURI: "http://localhost:8080",
	},
}
