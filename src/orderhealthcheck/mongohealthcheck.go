package orderhealthcheck

import (
	"context"
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type MongoDbHealthCheck struct {
	client *mongo.Client
}

func NewMongoHealthCheck(client *mongo.Client) MongoDbHealthCheck {
	return MongoDbHealthCheck{client: client}
}

func (m *MongoDbHealthCheck) Check() bool {
	err := m.client.Ping(context.Background(), readpref.Primary())
	if err != nil {
		log.Errorf("MongoHealthCheck : MongoDB connection down.")
		return false
	}
	return true
}
