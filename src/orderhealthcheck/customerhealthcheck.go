package orderhealthcheck

import (
	"orderservice/src/client"
)

type CustomerHealthCheck struct {
	client *client.Client
}

func NewCustomerHealthCheck(client *client.Client) CustomerHealthCheck {
	return CustomerHealthCheck{client: client}
}

func (c *CustomerHealthCheck) Check() bool {
	return c.client.GetCustomerServiceHealthy()
}
