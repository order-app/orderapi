package main

import (
	"orderservice/src/cmd"
)

func main() {
	cmd.Execute()
}
