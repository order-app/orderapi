package handler

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"orderservice/src/handler/type"
	"orderservice/src/pkg/healthcheck"
)

type HealthyHandler struct {
	HealthChecks healthcheck.HealthChecks
}

func NewHealthyHandler(checks healthcheck.HealthChecks) HealthyHandler {
	return HealthyHandler{HealthChecks: checks}
}

func (h *HealthyHandler) CheckServicesHealth(c echo.Context) error {
	res := h.HealthChecks.CheckServices()
	var response _type.HealthCheckResponse
	var serviceRes _type.ServicesHealthyResponse
	response.Status = healthcheck.Healthy.String()
	for name, health := range res {
		serviceRes.ServiceName = name
		for h, t := range health {
			if h == true {
				serviceRes.Status = healthcheck.Healthy.String()
			} else {
				serviceRes.Status = healthcheck.Unhealthy.String()
				response.Status = healthcheck.Unhealthy.String()
			}
			serviceRes.LastCheckTime = t
		}
		response.Services = append(response.Services, serviceRes)
	}
	return c.JSON(http.StatusOK, response)
}
