package handler

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"orderservice/src/client"
	"orderservice/src/handler/type"
	errors2 "orderservice/src/pkg/helper/errors"
	"orderservice/src/service"
	"orderservice/src/storage/helper"
)

type OrderHandler struct {
	service service.Service
	client  client.Client
}

func NewOrderHandler(service service.Service, client client.Client) OrderHandler {
	return OrderHandler{service: service, client: client}
}

// FindOrderById
//@Summary       Show an order
// @Description  get order by ID
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Order ID"
// @Success      200  {object}  _type.OrderResponseType
// @Failure		 404 {object} 	_type.ErrorType
// @Failure		 400 {object} 	_type.ErrorType
// @Router       /order/{id} [get]
func (h *OrderHandler) FindOrderById(c echo.Context) error {
	pID := c.Param("id")
	id, err := primitive.ObjectIDFromHex(pID)
	if err != nil {
		log.Errorf("Unable to convert to id (string to objectId) : %s", err)
		errors2.Panic(errors2.ConvertIdError)
	}
	res := h.service.FindById(id)
	return c.JSON(http.StatusOK, _type.NewOrderResponseType(res))
}

// FindOrders
// @Summary      List orders
// @Description  get orders
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        sort query string false "sort"
// @Param        page query int false "page"
// @Param		 customerId query string false "customerId"
// @Param		 quantity query int false "quantity"
// @Param		 price query float64 false "price"
// @Param		 product.name query string false "product name"
// @Param		 product.imageUrl query string false "product.imageUrl"
// @Param		 address.addressLine query string false "address line"
// @Param		 address.city query string false "city"
// @Param 		 address.country query string false "country"
// @Param		 address.cityCode query int false "city code"
// @Success      200  {object}  _type.FindManyResponseType
// @Failure		 404 {object} 	_type.ErrorType
// @Router       /order 	[get]
func (h *OrderHandler) FindOrders(c echo.Context) error {
	if cID := c.QueryParam("customerId"); cID != "" {
		h.client.GetIsCustomerIdValid(cID)
	}
	page, perPage, findOptions := helper.SetOptions(c.QueryParams())
	filter := helper.MakeFilter(c.QueryParams())
	total, res := h.service.FindOrders(filter, findOptions)
	return c.JSON(http.StatusOK, _type.NewFindManyResponseType(total, page, int(perPage), *res))
}

// CreateOrder
//@Summary       Add an order
// @Description  create order
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param 	_type.RequestOrderType body _type.RequestOrderType true "For create an Order"
//@Success      200  {object}  _type.IdResponseType
// @Failure		 400 {object} 	_type.ErrorType
// @Failure		 500 {object} 	_type.ErrorType
// @Router       /order [post]
func (h *OrderHandler) CreateOrder(c echo.Context) error {
	var orderReq _type.RequestOrderType
	if bindErr := c.Bind(&orderReq); bindErr != nil {
		log.Errorf("Unable to bind the request body : %s", bindErr)
		errors2.Panic(errors2.OrderBindError)
	}
	orderReq.ValidateRequest()
	h.client.GetIsCustomerIdValid(orderReq.CustomerId.Hex())
	order := orderReq.NewOrderToRequestType()
	res := h.service.InsertOrder(order)
	return c.JSON(http.StatusCreated, _type.IdResponseType{Id: res})
}

// DeleteOrder
//@Summary       Delete an order
// @Description  delete order by ID
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Order ID"
// @Success      200 {object}   _type.QuickResponseType
//@Failure       400  {object}  _type.ErrorType
// @Failure		 404 {object} 	_type.ErrorType
// @Router       /order/{id} [delete]
func (h *OrderHandler) DeleteOrder(c echo.Context) error {
	pId := c.Param("id")
	reqId, hexErr := primitive.ObjectIDFromHex(pId)
	if hexErr != nil {
		log.Errorf("Unable to convert to id (string to objectId) : %s", hexErr)
		errors2.Panic(errors2.ConvertIdError)
	}
	h.service.DeleteOneOrder(reqId)
	return c.JSON(http.StatusOK, _type.QuickResponseType{Result: true})
}

// UpdateOrder
//@Summary       Update an order
// @Description  update order
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Order ID"
// @Param 		_type.RequestOrderType body _type.RequestOrderType true "For update an Order"
// @Success      200  {object}  _type.QuickResponseType
// @Failure		 404 {object} 	_type.ErrorType
// @Failure		 400 {object} 	_type.ErrorType
// @Failure		 500 {object} 	_type.ErrorType
// @Router       /order/{id} [put]
func (h *OrderHandler) UpdateOrder(c echo.Context) error {
	pID := c.Param("id")
	reqId, hexErr := primitive.ObjectIDFromHex(pID)
	if hexErr != nil {
		log.Errorf("Unable to convert to id (string to objectId) : %s", hexErr)
		errors2.Panic(errors2.ConvertIdError)
	}
	var orderReq _type.RequestOrderType
	if bindErr := c.Bind(&orderReq); bindErr != nil {
		log.Errorf("Unable to bind the request body : %s", bindErr)
		errors2.Panic(errors2.OrderBindError)
	}
	orderReq.ValidateRequest()
	h.client.GetIsCustomerIdValid(orderReq.CustomerId.Hex())
	order := orderReq.NewOrderToRequestType()
	order.Id = reqId
	h.service.UpdateOrder(order)
	return c.JSON(http.StatusOK, _type.QuickResponseType{Result: true})
}

// ChangeOrderStatus
//@Summary       Update an order status
// @Description  update order status
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Order ID"
// @Param 		_type.ChangeStatusReq body _type.ChangeStatusReq true "For update an Order Status"
// @Success      200  {object}  _type.QuickResponseType
// @Failure		 404 {object} 	_type.ErrorType
// @Failure		 400 {object} 	_type.ErrorType
// @Router       /order/status/{id} [put]
func (h *OrderHandler) ChangeOrderStatus(c echo.Context) error {
	pID := c.Param("id")
	reqId, hexErr := primitive.ObjectIDFromHex(pID)
	if hexErr != nil {
		log.Errorf("Unable to convert to id (string to objectId) : %s", hexErr)
		errors2.Panic(errors2.ConvertIdError)
	}
	var statusReq _type.ChangeStatusReq
	if bindErr := c.Bind(&statusReq); bindErr != nil {
		log.Errorf("Unable to bind the request body : %s", bindErr)
		errors2.Panic(errors2.OrderBindError)
	}
	statusReq.ValidateRequest()
	update := helper.SetUpdateForStatus(statusReq.Status)
	h.service.ChangeStatus(reqId, update)
	return c.JSON(http.StatusOK, _type.QuickResponseType{Result: true})
}
