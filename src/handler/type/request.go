package _type

import (
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/bson/primitive"
	errors2 "orderservice/src/pkg/helper/errors"
	"orderservice/src/storage/storageType"
	"strings"
	"time"
)

type RequestOrderType struct {
	CustomerId primitive.ObjectID `json:"customerId" bson:"customerId"`
	Quantity   int                `json:"quantity" bson:"quantity"`
	Price      float64            `json:"price" bson:"price"`
	Address    Address            `json:"address" bson:"address"`
	Product    ProductReqRes      `json:"product" bson:"product"`
}

type Address struct {
	AddressLine string `json:"addressLine" bson:"addressLine"`
	City        string `json:"city" bson:"city"`
	Country     string `json:"country" bson:"country"`
	CityCode    int    `json:"cityCode" bson:"cityCode"`
}

type ProductReqRes struct {
	ImageUrl string `json:"imageUrl" bson:"imageUrl"`
	Name     string `json:"name" bson:"name"`
}

type ChangeStatusReq struct {
	Status string `json:"status" bson:"status"`
}

func (order *RequestOrderType) NewOrderToRequestType() storageType.Order {
	return storageType.Order{
		CustomerId: order.CustomerId,
		Quantity:   order.Quantity,
		Price:      order.Price,
		Status:     storageType.Pending,
		Address: storageType.Address{
			AddressLine: order.Address.AddressLine,
			Country:     order.Address.Country,
			City:        order.Address.City,
			CityCode:    order.Address.CityCode,
		},
		Product: storageType.Product{
			ImageUrl: order.Product.ImageUrl,
			Name:     order.Product.Name,
		},
		CreatedAt: time.Time{},
		UpdatedAt: time.Time{},
	}
}

func (order *RequestOrderType) ValidateRequest() {
	if order.Quantity == 0 || order.Quantity < 0 {
		log.Errorf("Quantity is required, it can not be lower than 1.")
		errors2.Panic(errors2.QuantityValidationError)
	}
	if order.Price == 0 || order.Price < 0 {
		log.Errorf("Price is required, it can not be lower than 1.")
		errors2.Panic(errors2.PriceValidationError)
	}
	if order.Product.Name == "" {
		log.Errorf("ProductName is required, it can not be empty.")
		errors2.Panic(errors2.ProductNameValidationError)
	}
	if order.Product.ImageUrl == "" {
		log.Errorf("ImageUrl is required, it can not be empty.")
		errors2.Panic(errors2.ImageUrlValidationError)
	}
	if order.Address.AddressLine == "" {
		log.Errorf("AddressLine is required, it can not be empty.")
		errors2.Panic(errors2.AddressLineValidationError)
	}
	if order.Address.Country == "" {
		log.Errorf("Country is required, it can not be empty.")
		errors2.Panic(errors2.CountryValidationError)
	}
	if order.Address.City == "" {
		log.Errorf("City is required, it can not be empty.")
		errors2.Panic(errors2.CityValidationError)
	}
	if order.Address.CityCode == 0 || order.Address.CityCode < 0 {
		log.Errorf("CityCode is required, it can not be lower than 1.")
		errors2.Panic(errors2.CityCodeValidationError)
	}
}

func (s *ChangeStatusReq) ValidateRequest() {
	if s.Status == strings.ToLower(storageType.Pending.String()) {
	} else if s.Status == strings.ToLower(storageType.Active.String()) {
	} else if s.Status == strings.ToLower(storageType.Suspended.String()) {
	} else if s.Status == strings.ToLower(storageType.Rejected.String()) {
	} else {
		log.Errorf("Status field can take : pending, active, suspended, rejected")
		errors2.Panic(errors2.ChangeStatusValidationError)
	}
}
