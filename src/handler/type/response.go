package _type

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"orderservice/src/storage/storageType"
	"time"
)

type OrderResponseType struct {
	CustomerId primitive.ObjectID `json:"customerId" bson:"customerId"`
	Quantity   int                `json:"quantity" bson:"quantity"`
	Price      float64            `json:"price" bson:"price"`
	Status     storageType.Status `json:"status" bson:"status"`
	Address    Address            `json:"address" bson:"address"`
	Product    ProductReqRes      `json:"product" bson:"product"`
}

type FindManyResponseType struct {
	TotalItemCount int                 `json:"totalItemCount"`
	PageNumber     int                 `json:"pageNumber"`
	PageSize       int                 `json:"pageSize"`
	Data           []OrderResponseType `json:"data"`
}

type QuickResponseType struct {
	Result bool `json:"result" bson:"result"`
}

type IdResponseType struct {
	Id interface{} `json:"id"`
}

type ErrorType struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type HealthCheckResponse struct {
	Status   string                    `json:"status"`
	Services []ServicesHealthyResponse `json:"services"`
}

type ServicesHealthyResponse struct {
	ServiceName   string    `json:"serviceName"`
	Status        string    `json:"serviceStatus"`
	LastCheckTime time.Time `json:"LastCheckTime"`
}

func NewFindManyResponseType(total int64, pageNumber int, pageSize int, ordersReq []storageType.Order) *FindManyResponseType {
	var orderRes []OrderResponseType
	for _, order := range ordersReq {
		orderRes = append(orderRes, *NewOrderResponseType(&order))
	}
	return &FindManyResponseType{
		TotalItemCount: int(total),
		PageNumber:     pageNumber,
		PageSize:       pageSize,
		Data:           orderRes,
	}
}

func NewOrderResponseType(o *storageType.Order) *OrderResponseType {
	return &OrderResponseType{
		CustomerId: o.CustomerId,
		Quantity:   o.Quantity,
		Price:      o.Price,
		Status:     o.Status,
		Address: Address{
			AddressLine: o.Address.AddressLine,
			City:        o.Address.City,
			Country:     o.Address.Country,
			CityCode:    o.Address.CityCode,
		},
		Product: ProductReqRes{
			ImageUrl: o.Product.ImageUrl,
			Name:     o.Product.Name,
		},
	}
}
